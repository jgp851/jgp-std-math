#ifdef  JGPSTDMATH_EXPORTS 
#define JGPSTDMATH_DLLCALL __declspec(dllexport)   /* Should be enabled before compiling .dll project for creating .dll*/
#else
#define JGPSTDMATH_DLLCALL __declspec(dllimport)  /* Should be enabled in Application side for using already created .dll*/
#endif


JGPSTDMATH_DLLCALL short jgp_std_math_convertEndianShort(short value);